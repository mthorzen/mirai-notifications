package com.mirai.reservationnotification.model;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public class HotelNotification extends Notification {

    private String creditCardNumber;

    public HotelNotification(Reservation reservation) {
        super(reservation);
        this.creditCardNumber = reservation.getCreditCardNumber();
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    @Override
    public String toString() {
        return "HotelNotification{" +
                "creditCardNumber='" + creditCardNumber + '\'' +
                "} " + super.toString();
    }
}
