package com.mirai.reservationnotification.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public class Notification {

    private String reservationId;
    private String hotelName;
    private String city;
    private String firstName;
    private String surName;
    private LocalDateTime reservationDate;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;

    public Notification(Reservation reservation) {
        this.reservationId = reservation.getReservationId();
        this.hotelName = reservation.getHotelName();
        this.city = reservation.getCity();
        this.firstName = reservation.getFirstName();
        this.surName = reservation.getSurName();
        this.reservationDate = reservation.getReservationDate();
        this.checkInDate = toLocalDate(reservation.getCheckInDate());
        this.checkOutDate = toLocalDate(reservation.getCheckOutDate());
    }

    public String getReservationId() {
        return reservationId;
    }

    public void setReservationId(String reservationId) {
        this.reservationId = reservationId;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public LocalDateTime getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(LocalDateTime reservationDate) {
        this.reservationDate = reservationDate;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    private LocalDate toLocalDate(Date date) {
        if (date != null) {
            return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "reservationId='" + reservationId + '\'' +
                ", hotelName='" + hotelName + '\'' +
                ", city='" + city + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surName='" + surName + '\'' +
                ", reservationDate=" + reservationDate +
                ", checkInDate=" + checkInDate +
                ", checkOutDate=" + checkOutDate +
                '}';
    }
}
