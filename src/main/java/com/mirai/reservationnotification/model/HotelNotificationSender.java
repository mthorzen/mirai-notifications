package com.mirai.reservationnotification.model;

import java.util.logging.Logger;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public class HotelNotificationSender implements NotificationSender {

    private HotelNotification hotelNotification;

    public HotelNotificationSender(HotelNotification hotelNotification) {
        this.hotelNotification = hotelNotification;
    }

    @Override
    public void sendNotification() {
        Logger.getLogger(ClientNotificationSender.class.toString()).info("Starting thread to send fax");
        new Thread(new FaxSender(hotelNotification)).start();
    }

    private static class FaxSender implements Runnable {

        private HotelNotification hotelNotification;

        FaxSender(HotelNotification hotelNotification) {
            this.hotelNotification = hotelNotification;
        }

        @Override
        public void run() {
            Logger.getLogger(ClientNotificationSender.class.toString()).info("Sending fax to hotel: " + hotelNotification);
        }
    }

}
