package com.mirai.reservationnotification.model;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public interface NotificationSender {
    void sendNotification();

    public enum NotificationType {
        CLIENT, HOTEL
    }

    public static class Factory {
        private Reservation reservation;

        public Factory(Reservation reservation) {
            this.reservation = reservation;
        }

        public NotificationSender getSender(NotificationType type) {
            switch (type) {
                case HOTEL:
                    return new HotelNotificationSender(createHotelNotification(reservation));
                case CLIENT:
                    return new ClientNotificationSender(createClientNotification(reservation));
                default:
                    throw new RuntimeException("Not implemented type: " + type);
            }
        }

        private Notification createClientNotification(Reservation reservation) {
            return new Notification(reservation);
        }

        private HotelNotification createHotelNotification(Reservation reservation) {
            return new HotelNotification(reservation);
        }
    }
}
