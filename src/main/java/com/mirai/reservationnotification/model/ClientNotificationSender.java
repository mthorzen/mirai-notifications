package com.mirai.reservationnotification.model;

import java.util.logging.Logger;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public class ClientNotificationSender implements NotificationSender {

    private Notification clientNotification;

    public ClientNotificationSender(Notification clientNotification) {
        this.clientNotification = clientNotification;
    }

    @Override
    public void sendNotification() {
        Logger.getLogger(ClientNotificationSender.class.toString()).info("Starting thread to send mail");
        new Thread(new EmailSender(clientNotification)).start();
    }

    private static class EmailSender implements Runnable {

        private Notification clientNotification;

        EmailSender(Notification clientNotification) {
            this.clientNotification= clientNotification;
        }

        @Override
        public void run() {
            Logger.getLogger(ClientNotificationSender.class.toString()).info("Sending email to client: " + clientNotification);
        }
    }

}
