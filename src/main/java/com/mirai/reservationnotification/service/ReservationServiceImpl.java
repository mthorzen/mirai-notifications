package com.mirai.reservationnotification.service;

import com.mirai.reservationnotification.model.Reservation;
import org.springframework.stereotype.Service;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    private NotificationService notificationService;

    public ReservationServiceImpl(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public void makeReservation(Reservation reservation) {
        // ... dao etc ...
        notificationService.sendNotifications(reservation);
    }
}
