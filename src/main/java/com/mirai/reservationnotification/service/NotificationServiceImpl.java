package com.mirai.reservationnotification.service;

import com.mirai.reservationnotification.model.Notification;
import com.mirai.reservationnotification.model.NotificationSender;
import com.mirai.reservationnotification.model.Reservation;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    @Override
    public void sendNotifications(Reservation reservation) {
        Logger.getLogger(NotificationServiceImpl.class.toString()).info("Sending notifications for reservation: " + reservation);
        NotificationSender.Factory factory = new NotificationSender.Factory(reservation);
        factory.getSender(NotificationSender.NotificationType.CLIENT).sendNotification();
        factory.getSender(NotificationSender.NotificationType.HOTEL).sendNotification();
    }

    @Override
    public void sendNotifications(List<Reservation> reservations) {
        reservations.stream()
                .sorted(Comparator.comparing(Reservation::getReservationDate).reversed())
                .forEach(r -> sendNotifications(r));
    }

}
