package com.mirai.reservationnotification.service;

import com.mirai.reservationnotification.model.Notification;
import com.mirai.reservationnotification.model.Reservation;

import java.util.List;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public interface NotificationService {

    void sendNotifications(Reservation reservation);

    void sendNotifications(List<Reservation> notifications);

}
