package com.mirai.reservationnotification.service;

import com.mirai.reservationnotification.model.Reservation;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
public interface ReservationService {

    void makeReservation(Reservation reservation);

}
