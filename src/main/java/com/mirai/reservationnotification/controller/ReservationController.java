package com.mirai.reservationnotification.controller;

import com.mirai.reservationnotification.model.Reservation;
import com.mirai.reservationnotification.service.ReservationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

/**
 * Created by mthorzen on 06/06/2018.
 *
 * @author mthorzen
 * @since 06/06/2018
 */
@Controller
public class ReservationController {

    private ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/")
    public String index(Model model) {
        Reservation reservation = new Reservation();
        reservation.setReservationId(UUID.randomUUID().toString());
        model.addAttribute("reservation", reservation);
        return "notify";
    }

    @PostMapping("/reservation")
    public String reservationSubmit(@ModelAttribute Reservation reservation, Model model) {
        reservationService.makeReservation(reservation);
        model.addAttribute("reservation", reservation);
        return "reservation";
    }

}
